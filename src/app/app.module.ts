import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GlobLogComponent } from './glob-log/glob-log.component';

import {DataService} from './data.service';

@NgModule({
  declarations: [
    AppComponent,
    GlobLogComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
