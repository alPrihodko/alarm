import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-glob-log',
  templateUrl: './glob-log.component.html',
  styleUrls: ['./glob-log.component.css']
})
export class GlobLogComponent implements OnInit {

  log = {
    message: "first log mess"
  };

  constructor(private dataService:DataService) {

  }

  ngOnInit() {
    console.log("init component");
    console.log(this.dataService.logs);
  }

}
