import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobLogComponent } from './glob-log.component';

describe('GlobLogComponent', () => {
  let component: GlobLogComponent;
  let fixture: ComponentFixture<GlobLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
