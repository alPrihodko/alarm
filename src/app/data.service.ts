import { Injectable } from '@angular/core';

@Injectable()

export class DataService {


    logs = [
      "logline1",
      "logline2"
    ];

  timer;

  constructor() {
    this.doLog();

    //this.timer = setTimeout(this.doLog, 1000);
  }

  someLocalMethod() {
    console.log("Local method");
  };

  doLog() {
    setTimeout(function run(self) {
      self.someLocalMethod();
      setTimeout(run, 1000, self);
    }, 1000, this);
  }

}
